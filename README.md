# Restaurant API server

You have to write a simple web application that implements a REST API.
The goal of this app is to choose a restaurant

## Endpoints

The endpoints you have to implement are :

    POST /restaurants
    {'name': 'Burger1'}

Add a restaurant to the list of known restaurant. The restaurant list needs to be stored to a persistent storage.

    DELETE /restaurants/<name>

Delete a restaurant from the list

    GET /restaurants

List all the restaurants

    GET /restaurants/random

Pick a random restaurant from the list

## Run the tests
    virtualenv venv -p python3
    source venv/bin/activate
    pip install -r requirements.txt
    pytest test
    
### Get the test coverage

Just do the previous and run `pytest --cov=api test`