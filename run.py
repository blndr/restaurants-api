from api.app import create_app

config = {
    'SQLALCHEMY_DATABASE_URI': 'sqlite:////tmp/test.db',
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'TESTING': False,
    'SECRET_KEY': 'AZERTY123',
    'ENV': 'DEV',
}

app = create_app(config)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)
