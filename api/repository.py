from typing import List, Optional

from api.models import Restaurant, db


def list_all_restaurants() -> List[Restaurant]:
    return Restaurant.query.all()


def save_restaurant(restaurant: Restaurant) -> None:
    db.session.add(restaurant)
    db.session.commit()


def find_by_name(name: str) -> Optional[Restaurant]:
    return Restaurant.query.filter_by(name=name).first()


def remove(restaurant: Restaurant) -> None:
    db.session.delete(restaurant)
    db.session.commit()
