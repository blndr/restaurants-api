import random
from typing import List

from api.models import Restaurant


def create_restaurant(raw_data: dict) -> Restaurant:
    restaurant = Restaurant()
    restaurant.name = raw_data['name']
    restaurant.signature_dish = raw_data['signature_dish']
    return restaurant


def random_pick(restaurants: List[Restaurant]) -> Restaurant:
    return random.choice(restaurants)
