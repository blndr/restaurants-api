import logging

from flask import Flask

import api
from api.models import db


def create_app(config: dict) -> Flask:
    logging.basicConfig(format='%(asctime)s :: %(levelname)s :: %(name)s :: %(message)s')

    app = Flask(__name__)
    app.config.update(config)
    app.app_context().push()

    api.register_routes()

    db.init_app(app)
    db.create_all()
    return app
