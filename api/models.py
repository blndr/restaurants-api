from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Restaurant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    signature_dish = db.Column(db.String, nullable=True)

    def serialize(self) -> dict:
        return {'name': self.name, 'signature_dish': self.signature_dish}
