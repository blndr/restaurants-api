from flask import request, current_app as app, jsonify

from api import repository, domain


@app.route('/restaurants', methods=['POST'])
def create_restaurant():
    new_restaurant = domain.create_restaurant(request.json)
    repository.save_restaurant(new_restaurant)
    return jsonify(new_restaurant.serialize()), 201


@app.route('/restaurants/<name>', methods=['DELETE'])
def delete_restaurant(name: str):
    restaurant = repository.find_by_name(name)

    if not restaurant:
        return '', 404

    repository.remove(restaurant)
    return '', 204


@app.route('/restaurants', methods=['GET'])
def get_restaurants():
    restaurants = repository.list_all_restaurants()
    return jsonify([r.serialize() for r in restaurants]), 200


@app.route('/restaurants/random', methods=['GET'])
def get_random_restaurant():
    restaurants = repository.list_all_restaurants()

    if not restaurants:
        return '', 204

    picked_restaurant = domain.random_pick(restaurants)
    return jsonify(picked_restaurant.serialize()), 200
