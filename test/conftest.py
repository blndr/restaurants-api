from functools import wraps

import pytest

from api.app import create_app
from api.models import Restaurant


@pytest.fixture(scope='session')
def client():
    config = {
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///:memory:',
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'TESTING': True,
        'SECRET_KEY': '34u4&&$%-030-243**'';..<><',
        'ENV': 'DEV'
    }

    app = create_app(config)
    client = app.test_client()
    yield client


def clean_database(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        Restaurant.query.delete()
        return f(*args, **kwargs)

    return decorated_function
