from api.models import Restaurant


def create_restaurant(name="L'Arpège", signature_dish="Mi-poulet mi-canard"):
    restaurant = Restaurant()
    restaurant.name = name
    restaurant.signature_dish = signature_dish
    return restaurant
