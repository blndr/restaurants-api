from api import repository
from test import fixture, API_ROOT
from test.conftest import clean_database


@clean_database
def test_returns_204(client):
    # given
    restaurant = fixture.create_restaurant()
    repository.save_restaurant(restaurant)

    # when
    response = client.delete(API_ROOT + '/restaurants/%s' % restaurant.name)

    # then
    assert response.status_code == 204


@clean_database
def test_removes_the_restaurant_from_storage(client):
    # given
    restaurant = fixture.create_restaurant()
    repository.save_restaurant(restaurant)

    # when
    client.delete(API_ROOT + '/restaurants/%s' % restaurant.name)

    # then
    assert len(repository.list_all_restaurants()) == 0


@clean_database
def test_returns_404_if_restaurant_is_unknown(client):
    # given
    restaurant = fixture.create_restaurant()
    repository.save_restaurant(restaurant)

    # when
    response = client.delete(API_ROOT + '/restaurants/%s' % 'Restaurant inconnu')

    # then
    assert response.status_code == 404
