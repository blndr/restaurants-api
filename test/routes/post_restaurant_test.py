from flask import json

from api import repository
from test import API_ROOT
from test.conftest import clean_database


@clean_database
def test_returns_201(client):
    # given
    new_restaurant = {
        'name': 'Ĺa bonne fourchette',
        'signature_dish': 'Blanquette de veau'
    }

    # when
    response = client.post(
        API_ROOT + '/restaurants',
        data=json.dumps(new_restaurant),
        content_type='application/json'
    )

    # then
    assert response.status_code == 201


@clean_database
def test_saves_a_new_restaurant(client):
    # given
    new_restaurant = {
        'name': 'Ĺa bonne fourchette',
        'signature_dish': 'Blanquette de veau'
    }

    # when
    client.post(
        API_ROOT + '/restaurants',
        data=json.dumps(new_restaurant),
        content_type='application/json'
    )

    # then
    persisted_restaurant = repository.list_all_restaurants()[0]
    assert persisted_restaurant.name == 'Ĺa bonne fourchette'
    assert persisted_restaurant.signature_dish == 'Blanquette de veau'


@clean_database
def test_returns_the_newly_created_restaurant_as_json(client):
    # given
    new_restaurant = {
        'name': 'Ĺa bonne fourchette',
        'signature_dish': 'Blanquette de veau'
    }

    # when
    response = client.post(
        API_ROOT + '/restaurants',
        data=json.dumps(new_restaurant),
        content_type='application/json'
    )

    # then
    assert response.json == {
        'name': 'Ĺa bonne fourchette',
        'signature_dish': 'Blanquette de veau'
    }
