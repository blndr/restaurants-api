from api import repository
from test import fixture, API_ROOT
from test.conftest import clean_database


@clean_database
def test_returns_200(client):
    # when
    response = client.get(API_ROOT + '/restaurants')

    # then
    assert response.status_code == 200


@clean_database
def test_returns_an_empty_list_if_no_restaurants_exist(client):
    # when
    response = client.get(API_ROOT + '/restaurants')

    # then
    assert response.json == []


@clean_database
def test_returns_all_the_restaurants_as_json(client):
    # given
    restaurant1 = fixture.create_restaurant(name="Restau 1", signature_dish="Dish 1")
    restaurant2 = fixture.create_restaurant(name="Restau 2", signature_dish="Dish 2")
    repository.save_restaurant(restaurant1)
    repository.save_restaurant(restaurant2)

    # when
    response = client.get(API_ROOT + '/restaurants')

    # then
    assert response.json == [
        {'name': 'Restau 1', 'signature_dish': 'Dish 1'},
        {'name': 'Restau 2', 'signature_dish': 'Dish 2'}
    ]
