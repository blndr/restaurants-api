from unittest.mock import patch

from api import repository
from test import fixture, API_ROOT
from test.conftest import clean_database


@clean_database
@patch('api.domain.random_pick')
def test_returns_200(mocked_random_pick, client):
    # given
    restaurant1 = fixture.create_restaurant(name="Restau 1", signature_dish="Dish 1")
    restaurant2 = fixture.create_restaurant(name="Restau 2", signature_dish="Dish 2")
    repository.save_restaurant(restaurant1)
    repository.save_restaurant(restaurant2)
    mocked_random_pick.return_value = restaurant2

    # when
    response = client.get(API_ROOT + '/restaurants/random')

    # then
    assert response.status_code == 200


@clean_database
@patch('api.domain.random_pick')
def test_returns_a_random_restaurant(mocked_random_pick, client):
    # given
    restaurant1 = fixture.create_restaurant(name="Restau 1", signature_dish="Dish 1")
    restaurant2 = fixture.create_restaurant(name="Restau 2", signature_dish="Dish 2")
    repository.save_restaurant(restaurant1)
    repository.save_restaurant(restaurant2)
    mocked_random_pick.return_value = restaurant2

    # when
    response = client.get(API_ROOT + '/restaurants/random')

    # then
    assert response.json == {'name': 'Restau 2', 'signature_dish': 'Dish 2'}


@clean_database
def test_returns_204_if_no_restaurants_exist(client):
    # when
    response = client.get(API_ROOT + '/restaurants/random')

    # then
    assert response.status_code == 204
